# view AndroidManifest.xml #generated:15
-keep class com.tringtringlabs.mykidstory.App { <init>(...); }

# view AndroidManifest.xml #generated:39
-keep class com.tringtringlabs.mykidstory.ComposeStoryActivity { <init>(...); }

# view AndroidManifest.xml #generated:47
-keep class com.tringtringlabs.mykidstory.ui.AddKidActivity { <init>(...); }

# view AndroidManifest.xml #generated:20
-keep class com.tringtringlabs.mykidstory.ui.KidFeedActivity { <init>(...); }

# view AndroidManifest.xml #generated:29
-keep class com.tringtringlabs.mykidstory.ui.PresentationActivity { <init>(...); }

