package com.tringtringlabs.mykidstory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Environment;
import android.text.format.DateUtils;
import android.view.inputmethod.InputMethodManager;

import com.tringtringlabs.mykidstory.components.MemCache;
import com.tringtringlabs.mykidstory.ui.StoriesCarouselHelper;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

public class Common {

	public static void handleCopyFilesToSdCard(final Context context) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				File targetFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getString(R.string.app_name)
						+ "/");
				deleteAllFiles(targetFolder);
				if (!targetFolder.exists()) {
					targetFolder.mkdirs();
				}
				File privateFilesRoot = new File("/data/data/" + context.getPackageName());
				copyFiles(privateFilesRoot, context);
			}
		});
		thread.setName("DevCopyFiles");
		thread.start();
	}

	private static void deleteAllFiles(File targetFolder) {
		File[] filesList = targetFolder.listFiles();
		if (filesList == null) {
			return;
		}
		for (File file : filesList) {
			if (file.isDirectory()) {
				deleteAllFiles(file);
				file.delete();
			} else {
				file.delete();
			}
		}

	}

	private static void copyFiles(File file, Context context) {
		if (!file.exists()) {
			return;
		}
		if (file.isDirectory()) {
			File[] listFiles = file.listFiles();
			for (File afile : listFiles) {
				copyFiles(afile, context);
			}

		} else {
			File targetFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + context.getString(R.string.app_name) + "/"
					+ file.getAbsolutePath().replace("/data/data/" + context.getPackageName(), ""));
			File parentFile = targetFile.getParentFile();
			if (!parentFile.exists()) {
				parentFile.mkdirs();
			}
			if (!targetFile.exists()) {
				try {
					targetFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				OutputStream oStream = new FileOutputStream(targetFile);
				FileInputStream iStream = new FileInputStream(file);
				byte[] buffer = new byte[512];
				int length = 0;
				while ((length = iStream.read(buffer)) > 0) {
					oStream.write(buffer, 0, length);
				}
				oStream.close();
				iStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static int getPackageVersion() {
		App app = App.getApp();
		try {
			PackageInfo packageInfo = app.getPackageManager().getPackageInfo(app.getPackageName(), PackageManager.GET_META_DATA);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static void comingSoon(Activity activity) {
		Crouton.showText(activity, "Coming Soon", Style.INFO);
	}

	public static CharSequence getDateText(int year, int monthOfYear, int dayOfMonth) {
		return dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
	}

	public static CharSequence getTimeText(int hourOfDay, int minute) {
		return hourOfDay + ":" + minute;
	}

	public static void hideKeyBoard(Activity mActivity) {
		InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (null != mActivity.getCurrentFocus()) {
			inputManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	public static Bitmap getBitmap(String localFilePath) {
		Bitmap bitmap = MemCache.imageCache.get(localFilePath);
		if(bitmap == null) {
			Options options = new Options();
			options.outWidth = StoriesCarouselHelper.TEXTURE_WIDTH;
			options.outHeight = StoriesCarouselHelper.TEXTURE_HEIGHT;
			options.inMutable = true;
			bitmap = BitmapFactory.decodeFile(localFilePath, options);
			MemCache.imageCache.put(localFilePath, bitmap);
		}
		return bitmap;
	}

	public static CharSequence getRelativeDate(Date createdAt) {
		return DateUtils.getRelativeDateTimeString(App.getContext(), createdAt.getTime(), DateUtils.DAY_IN_MILLIS, DateUtils.DAY_IN_MILLIS,
				DateUtils.FORMAT_ABBREV_MONTH);
	}

}
