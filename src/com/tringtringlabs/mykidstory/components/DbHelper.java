package com.tringtringlabs.mykidstory.components;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.tringtringlabs.mykidstory.App;

public class DbHelper extends SQLiteOpenHelper {

	public static final String LOCAL_FILE_PATH = "local_file_path";
	public static final String KID_ID = "kid_id";
	public static final String OTHER_ATTRS = "other_attrs";
	public static final String STORY_TYPE = "story_type";
	public static final String BIRTH_LOC_LONG = "birthLocLong";
	public static final String BIRTH_LOC_LAT = "birthLocLat";
	public static final String BIRTH_LOCATION = "birthLocation";
	public static final String STORY_DATE = "storyDate";
	public static final String UPDATED_TIME = "updatedTime";
	public static final String CREATED_TIME = "createdTime";
	public static final String KID_TEETH = "kid_teeth";
	public static final String KID_GROWTH = "kid_growth";
	public static final String PROFILE_PICTURE = "profile_picture";
	public static final String MOTHER_ID = "mother_id";
	public static final String FATHER_ID = "father_id";
	public static final String SIZE = "size";
	public static final String DURATION = "duration";
	public static final String TYPE = "type";
	public static final String URL = "url";
	public static final String OWNER_ID = "ownerId";
	public static final String STORY_ID = "storyId";
	public static final String FB_POST_ID = "fbPostId";
	public static final String TAGS = "tags";
	public static final String DESCRIPTION = "description";
	public static final String TITLE = "title";
	public static final String LOCATION_LONG = "locationLong";
	public static final String LOCATION_LAT = "locationLat";
	public static final String LOCATION = "location";
	public static final String JOB_TITLE = "jobTitle";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String FB_ID = "fbId";
	public static final String EMAIL = "email";
	public static final String GRAND_PARENT_PA = "grandParentPa";
	public static final String GRAND_PARENT_MOM = "grandParentMom";
	public static final String IS_PARENT = "isParent";
	public static final String IS_DEVICE_OWNER = "isDeviceOwner";
	public static final String NUMBER_OF_TEETH = "numberOfTeeth";
	public static final String HEIGHT_IN_CMS = "heightInCms";
	public static final String MEASURED_DATE = "measuredDate";
	public static final String IS_PRIME = "isPrime";
	public static final String BIRTH_SIGN = "birthSign";
	public static final String GENDER = "gender";
	public static final String DOB = "dob";
	public static final String NAME = "name";
	public static final String OBJECT_ID = "objectId";
	public static String db_name = "kidline.db";
	public static int version = 1;

	private static final String CREATE_TABLE = "CREATE TABLE ";
	public static final String TBL_KID = "kid";
	public static final String TBL_PARENT_X = "parent_x";
	public static final String TBL_GRAND_PARENT_X = "grand_parent_x";
	public static final String TBL_STORY = "story";
	public static final String TBL_STORY_RICH_MEDIA = "rich_media";
	
	// Create table queries
	private static final String CREATE_TABLE_KID = CREATE_TABLE + TBL_KID + "(" + OBJECT_ID + " TEXT PRIMARY KEY, " + NAME + " TEXT, "
			+ PROFILE_PICTURE + " BLOB," + DOB + " INT, " + GENDER + " SMALLINT DEFAULT 1, " + EMAIL + " TEXT, " + FB_ID + " TEXT, " + PHONE_NUMBER
			+ " TEXT, " + JOB_TITLE + " TEXT, " + LOCATION + " TEXT, " + LOCATION_LAT + " TEXT, " + LOCATION_LONG + " TEXT," + BIRTH_SIGN + " TEXT, "
			+ FATHER_ID + " TEXT, " + MOTHER_ID + " TEXT," + IS_PRIME + " SMALLINT DEFAULT 0, " + KID_GROWTH + " TEXT, " + KID_TEETH + " TEXT, "
			+ BIRTH_LOCATION + " TEXT, " + BIRTH_LOC_LAT + " TEXT, " + BIRTH_LOC_LONG + " TEXT, " + CREATED_TIME + " INT, " + UPDATED_TIME + " INT)";
	private static final String CREATE_TABLE_PARENT_X = CREATE_TABLE + TBL_PARENT_X + "(" + OBJECT_ID + " TEXT PRIMARY KEY, " + IS_DEVICE_OWNER + " SMALLINT DEFAULT 0, " + IS_PARENT + " SMALLINT DEFAULT 0, " + GRAND_PARENT_MOM + " TEXT, " + GRAND_PARENT_PA + " TEXT, " + NAME + " TEXT, " + PROFILE_PICTURE + " BLOB," + DOB + " INT," + GENDER + " SMALLINT DEFAULT 1, " + EMAIL + " TEXT, " + FB_ID + " TEXT, " + PHONE_NUMBER + " TEXT, " + JOB_TITLE + " TEXT, " + LOCATION + " TEXT, " + LOCATION_LAT + " TEXT, " + LOCATION_LONG + " TEXT," + CREATED_TIME + " INT, " + UPDATED_TIME + " INT)";
	private static final String CREATE_TABLE_GRAND_PARENT_X = CREATE_TABLE + TBL_GRAND_PARENT_X + "(" + OBJECT_ID + " TEXT PRIMARY KEY, " + NAME + " TEXT, " + PROFILE_PICTURE + " BLOB," + DOB + " INT," + GENDER + " SMALLINT DEFAULT 1, " + EMAIL + " TEXT, " + FB_ID + " TEXT, " + PHONE_NUMBER + " TEXT, " + JOB_TITLE + " TEXT, " + LOCATION + " TEXT, " + LOCATION_LAT + " TEXT, " + LOCATION_LONG + " TEXT," + CREATED_TIME + " INT, " + UPDATED_TIME + " INT)";
	private static final String CREATE_TABLE_STORY = CREATE_TABLE + TBL_STORY + "(" + OBJECT_ID + " TEXT PRIMARY KEY , " + KID_ID + " TEXT," + STORY_TYPE + " SMALLINT DEFAULT 0," + TITLE + " TEXT, " + DESCRIPTION + " TEXT, " + STORY_DATE + " INT," + TAGS + " TEXT, " + LOCATION + " TEXT, " + LOCATION_LAT + " TEXT, " + LOCATION_LONG + " TEXT, " + FB_POST_ID + " TEXT, " + OTHER_ATTRS + " TEXT, " + CREATED_TIME + " INT, " + UPDATED_TIME + " INT)";
	private static final String CREATE_TABLE_STORY_RICH_MEDIA = CREATE_TABLE + TBL_STORY_RICH_MEDIA + "(" + OBJECT_ID + " TEXT PRIMARY KEY, "
			+ STORY_ID + " TEXT, " + OWNER_ID + " TEXT, " + URL + " TEXT, " + LOCAL_FILE_PATH + " TEXT, " + LOCATION_LAT + " TEXT, " + LOCATION_LONG + " TEXT, " + TYPE + " TEXT, " + DURATION + " INT, "
			+ SIZE + " INT, " + CREATED_TIME + " INT, " + UPDATED_TIME + " INT)";
	private static final String NULL = "null";
	
	private static DbHelper instance;
	private final SQLiteDatabase mDb;
	
	private DbHelper() {
		super(App.getContext(), db_name, null, version);
		instance = this;
		mDb = getWritableDatabase();
	}
	
	@Override
	protected void finalize() throws Throwable {
		if (mDb != null && mDb.isOpen()) {
			mDb.close();
		}
		super.finalize();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		GLogger.v(CREATE_TABLE_KID);
		//GLogger.v(CREATE_TABLE_KID_GROWTH);
		//GLogger.v(CREATE_TABLE_KID_TEETH);
		GLogger.v(CREATE_TABLE_PARENT_X);
		GLogger.v(CREATE_TABLE_GRAND_PARENT_X);
		GLogger.v(CREATE_TABLE_STORY);
		GLogger.v(CREATE_TABLE_STORY_RICH_MEDIA);
		db.execSQL(CREATE_TABLE_KID);
		//db.execSQL(CREATE_TABLE_KID_GROWTH);
		//db.execSQL(CREATE_TABLE_KID_TEETH);
		db.execSQL(CREATE_TABLE_PARENT_X);
		db.execSQL(CREATE_TABLE_GRAND_PARENT_X);
		db.execSQL(CREATE_TABLE_STORY);
		db.execSQL(CREATE_TABLE_STORY_RICH_MEDIA);
		ParseManager.updateInstallationDetails();
	}
	
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
	public static DbHelper getInstance() {
		if (instance == null) {
			instance = new DbHelper();
		}
		return instance;
	}

	public synchronized void execSQL(String... sqlQueries) throws Exception {
		if(mDb != null && mDb.isOpen()) {
			for (String sql : sqlQueries) {
				GLogger.d("sql: " + sql);
				mDb.execSQL(sql);
			}
		} else {
			throw new Exception("Failed to open the database.");
		}
	}
	
	public synchronized void insertAsStatementWithBlob(String sql, byte[] bytes) {
		SQLiteStatement statement = getWritableDatabase().compileStatement(sql);
		if (sql.contains("?") && bytes != null) {
			statement.bindBlob(1, bytes);
		}
		statement.executeInsert();
	}
	
	public synchronized Cursor rawQuery(String sql) throws Exception {
		if(mDb != null && mDb.isOpen()) {
			Cursor rawQuery = mDb.rawQuery(sql, null);
			return rawQuery;
		} else {
			throw new Exception("Failed to open the database.");
		}
	}

	public static String nullCheckOut(String aString) {
		if (aString == null)
			return null;
		if (aString.equals(NULL)) {
			return null;
		}
		return aString;

	}


	public static String nullCheckIn(String aString) {
		if(aString == null) {
			return NULL;
		}
		return aString;
	}

}
