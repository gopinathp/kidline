package com.tringtringlabs.mykidstory.components;
/**
 * @author gopinath
 * A wrapper around the regular Android logger which will disable debug logs in production automatically.
 */
import android.util.Log;

import com.tringtringlabs.mykidstory.App;
import com.tringtringlabs.mykidstory.BuildConfig;

public class GLogger {

	private static final String TAG = App.getContext().getPackageName();

	public static void d(String msg) {
		if(BuildConfig.DEBUG)
			Log.d(TAG, msg);
	}
	
	public static void e(String msg) {
			Log.e(TAG, msg);
	}
	
	public static void w(String msg) {
			Log.w(TAG, msg);
	}
	
	public static void v(String msg) {
		if(BuildConfig.DEBUG)
			Log.v(TAG, msg);
	}
	
	public static void i(String msg) {
		if(BuildConfig.DEBUG)
			Log.i(TAG, msg);
	}
}
