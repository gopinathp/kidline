package com.tringtringlabs.mykidstory.components;

import android.os.Build;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.tringtringlabs.mykidstory.App;
import com.tringtringlabs.mykidstory.BuildConfig;
import com.tringtringlabs.mykidstory.Common;
import com.tringtringlabs.mykidstory.Config;

public class ParseManager {

	public static void initialize() {
		if(BuildConfig.DEBUG) {
			Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
			Parse.initialize(App.getContext(), Config.PARSE_DEV_APP_ID, Config.PARSE_DEV_APP_KEY);
		} else {
			Parse.setLogLevel(Parse.LOG_LEVEL_ERROR);
			Parse.initialize(App.getContext(), Config.PARSE_APP_ID, Config.PARSE_APP_KEY);
		}
	}

	public static void updateInstallationDetails() {
		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		installation.put("app_version", Common.getPackageVersion());
		installation.put("sdk_int", Build.VERSION.SDK_INT);
		installation.put("manufacturer", Build.MANUFACTURER);
		installation.put("model", Build.MODEL);
		installation.put("device", Build.DEVICE);
		installation.put("display", Build.DISPLAY);
		installation.saveEventually();
	}
	
}
