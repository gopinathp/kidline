package com.tringtringlabs.mykidstory.components.models;

public enum StoryType {
	COMING_HOME,
	BIRTH_DAY,
	FIRST_HAIR_CUT,
	FIRST_CRAWL,
	FIRST_SIT,
	FIRST_STAND,
	FIRST_WORDS,
	FIRST_DAY_AT_SCHOOL,
	OUTING,
	ACHIEVEMENT,
	OTHERS
}
