package com.tringtringlabs.mykidstory.components.models;

import static com.tringtringlabs.mykidstory.Constants.COMMA;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.ParseObject;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.utils.parseable;

public class Kid extends Person implements parseable{
	
	public class GrowthMeasure {
		int heightInCms;
		Date measureDate;
		int weightInGrams;
	}

	public class TeethRecording {
		Date measureDate;
		int numberOfTeeth;
	}

	private String birthLocation;
	private double birthLocLat = -1;
	private double birthLocLong = -1;
	private String birthSign;

	private String fatherId;
	private boolean isPrime;
	private ArrayList<GrowthMeasure> kidGrowth;
	private ArrayList<TeethRecording> kidTeeth;
	private String motherId;
	
	public Kid(Cursor cursor) {
		super(cursor);
		isPrime = (cursor.getInt(cursor.getColumnIndex(DbHelper.IS_PRIME)) == 1);
		birthSign = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.BIRTH_SIGN)));
		fatherId = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.FATHER_ID)));
		motherId = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.MOTHER_ID)));
		Gson gson = new Gson();
		
		String growthJson = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.KID_GROWTH)));
		if(growthJson != null) {
			Type growthType = new TypeToken<Collection<GrowthMeasure>>(){}.getType();
			ArrayList<GrowthMeasure> growths = gson.fromJson(growthJson, growthType);
			setKidGrowth(growths);
		}
		
		String teethRecordJson = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.KID_TEETH)));
		if (teethRecordJson != null) {
			Type teethRecordingType = new TypeToken<Collection<TeethRecording>>() {}.getType();
			ArrayList<TeethRecording> teethRecordings = gson.fromJson(teethRecordJson, teethRecordingType);
			setKidTeeth(teethRecordings);
		}
		setBirthLocation(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.BIRTH_LOCATION))));
		setBirthLocLat(cursor.getDouble(cursor.getColumnIndex(DbHelper.BIRTH_LOC_LAT)));
		setBirthLocLong(cursor.getDouble(cursor.getColumnIndex(DbHelper.BIRTH_LOC_LONG)));
		setCreatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.CREATED_TIME))));
		setUpdatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.UPDATED_TIME))));
	}

	public Kid(String id, String name, Gender gender, Date dateOfBirth, String email, String fbId, String location, String phoneNumber,
			String jobTitle, double locationLat, double locationLong, byte[] profilePicture, boolean isPrime, String birthSign, String fatherId,
			String motherId, ArrayList<GrowthMeasure> kidGrowth, ArrayList<TeethRecording> kidTeeth, String birthLocation, double birthLocLat, double birthLocLong) {
		super(id, name, gender, dateOfBirth, email, fbId, location, phoneNumber, jobTitle, locationLat, locationLong, profilePicture);
		this.isPrime = isPrime;
		this.birthSign = birthSign;
		this.fatherId = fatherId;
		this.motherId = motherId;
		this.kidGrowth = kidGrowth;
		this.kidTeeth = kidTeeth;
		this.birthLocation = birthLocation;
		this.birthLocLat = birthLocLat;
		this.birthLocLong = birthLocLong;
		Date now = new Date();
		setCreatedAt(now);
		setUpdatedAt(now);
	}

	public String getBirthLocation() {
		return birthLocation;
	}

	public double getBirthLocLat() {
		return birthLocLat;
	}

	public double getBirthLocLong() {
		return birthLocLong;
	}

	public String getBirthSign() {
		return birthSign;
	}

	public String getFatherId() {
		return fatherId;
	}

	public String getInsertSql() {
		String growthJson = null;
		if(kidGrowth != null) {
			growthJson = new Gson().toJson(kidGrowth);
		}
		String teethJson = null;
		if(kidTeeth != null) {
			teethJson = new Gson().toJson(kidTeeth); 
		}
		String sql = "INSERT INTO " + DbHelper.TBL_KID + " VALUES(" 
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getId())) + COMMA 
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getName())) + COMMA 
				+ ((getProfilePicture() == null) ? "null" : " ? ") + COMMA
				+ getDateOfBirth().getTime() + COMMA 
				+ getGender().ordinal() + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getEmail())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getFbId())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getPhoneNumber())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getJobTitle())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getLocation())) + COMMA
				+ getLocationLat() + COMMA
				+ getLocationLong() + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getBirthSign())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getFatherId())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getMotherId())) + COMMA
				+ (isPrime() ? 1 : 0) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(growthJson)) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(teethJson)) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(birthLocation)) + COMMA
				+ birthLocLat + COMMA
				+ birthLocLong + COMMA
				+ getCreatedAt().getTime() + COMMA
				+ getUpdatedAt().getTime()
				+ ")";
		return sql;
		
	}

	public ArrayList<GrowthMeasure> getKidGrowth() {
		return kidGrowth;
	}

	public ArrayList<TeethRecording> getKidTeeth() {
		return kidTeeth;
	}

	public String getMotherId() {
		return motherId;
	}

	public boolean isPrime() {
		return isPrime;
	}

	public void setBirthLocation(String birthLocation) {
		this.birthLocation = birthLocation;
	}

	public void setBirthLocLat(double birthLocLat) {
		this.birthLocLat = birthLocLat;
	}

	public void setBirthLocLong(double birthLocLong) {
		this.birthLocLong = birthLocLong;
	}

	public void setBirthSign(String birthSign) {
		this.birthSign = birthSign;
	}

	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}

	public void setKidGrowth(ArrayList<GrowthMeasure> kidGrowth) {
		this.kidGrowth = kidGrowth;
	}

	public void setKidTeeth(ArrayList<TeethRecording> kidTeeth) {
		this.kidTeeth = kidTeeth;
	}

	public void setMotherId(String motherId) {
		this.motherId = motherId;
	}

	public void setPrime(boolean isPrime) {
		this.isPrime = isPrime;
	}

	@Override
	public String toString() {
		//return new Gson().toJson(this);
		return getName();
	}
	
	public ParseObject toParseObject() throws IllegalArgumentException, IllegalAccessException {
		ParseObject object = new ParseObject(this.getClass().getName());
		Field[] declaredFields = getClass().getDeclaredFields();
		for (Field field : declaredFields) {
			object.put(field.getName(), field.get(this));
		}
		return object;
	}
}
