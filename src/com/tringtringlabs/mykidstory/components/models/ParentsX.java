package com.tringtringlabs.mykidstory.components.models;

import static com.tringtringlabs.mykidstory.Constants.COMMA;

import java.lang.reflect.Field;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.google.gson.Gson;
import com.parse.ParseObject;
import com.tringtringlabs.mykidstory.Constants;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.utils.parseable;

public class ParentsX extends Person implements parseable{
	boolean isDeviceOwner;
	boolean isParent;
	String grandParentMom;
	String grandParentPa;
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
	public ParentsX() {
	}
	
	public ParentsX(Cursor cursor) {
		super(cursor);
		isDeviceOwner = (cursor.getInt(cursor.getColumnIndex(DbHelper.IS_DEVICE_OWNER)) == 1);
		isParent = (cursor.getInt(cursor.getColumnIndex(DbHelper.IS_PARENT)) == 1);
		grandParentMom = cursor.getString(cursor.getColumnIndex(DbHelper.GRAND_PARENT_MOM));
		grandParentPa = cursor.getString(cursor.getColumnIndex(DbHelper.GRAND_PARENT_PA));
	}

	public String getInsertSql() {
		String sql = "INSERT INTO " + DbHelper.TBL_PARENT_X + " VALUES(" 
						+ getId() + Constants.COMMA
						+ (isDeviceOwner? 1:0) + COMMA
						+ (isParent? 1:0) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(grandParentMom)) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(grandParentPa)) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getName())) + COMMA
						+ ((getProfilePicture() == null) ? "null" : " ? ") + COMMA
						+ getDateOfBirth().getTime() + COMMA
						+ getGender() + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getEmail())) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getFbId())) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getPhoneNumber())) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getJobTitle())) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getLocation())) + COMMA
						+ getLocationLat() + COMMA
						+ getLocationLong() + COMMA
						+ getCreatedAt().getTime() + COMMA
						+ getUpdatedAt().getTime() + COMMA
						+")";
		return sql;
	}
	
	public ParseObject toParseObject() throws IllegalArgumentException, IllegalAccessException {
		ParseObject object = new ParseObject(this.getClass().getName());
		Field[] declaredFields = getClass().getDeclaredFields();
		for (Field field : declaredFields) {
			object.put(field.getName(), field.get(this));
		}
		return object;
	}
}
