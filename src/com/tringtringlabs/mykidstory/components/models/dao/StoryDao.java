package com.tringtringlabs.mykidstory.components.models.dao;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.RichMedia;
import com.tringtringlabs.mykidstory.components.models.Story;

public class StoryDao implements BaseDao<Story> {
	
	private static StoryDao instance;

	private StoryDao() {
		instance = this;
	}
	
	public static StoryDao getInstance() {
		if(instance == null) {
			new StoryDao();
		}
		return instance;
	}

	@Override
	public void add(Story story) throws Exception {
		String sql = story.getInsertSql();
		DbHelper.getInstance().execSQL(sql);
	}
	
	@Override
	public void add(ArrayList<Story> stories) throws Exception {
		String[] sqls = new String[stories.size()];;
		int i = 0;
		for (Story story : stories) {
			sqls[i++] = (story.getInsertSql());
		}
		DbHelper.getInstance().execSQL(sqls);
	}

	@Override
	public void update(String primaryKey, Story story) throws Exception {
		delete(primaryKey);
		add(story);
	}

	@Override
	public void delete(Story story) throws Exception {
		delete(story.getId());
	}
	
	@Override
	public void delete(String objectId) throws Exception {
		if(objectId == null) throw new Exception("ObjectId cannot be null");
		String deleteStory = "DELETE FROM " + DbHelper.TBL_STORY + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(objectId);
		String deleteStoryRichMedia = "DELETE FROM " + DbHelper.TBL_STORY_RICH_MEDIA + " WHERE " + DbHelper.STORY_ID + "=" + DatabaseUtils.sqlEscapeString(objectId);
		DbHelper.getInstance().execSQL(deleteStory, deleteStoryRichMedia);
	}

	@Override
	public Story findById(String id) throws Exception {
		String sql = " SELECT * from " + DbHelper.TBL_STORY + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(id);
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		if(cursor.moveToNext()) {
			Story story = new Story(cursor);
			cursor.close();
			return story;
		}
		return null;
	}

	@Override
	public ArrayList<Story> getAll() throws Exception {
		String sql = "SELECT * from " + DbHelper.TBL_STORY + " ORDER BY " + DbHelper.CREATED_TIME + " DESC";
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		ArrayList<Story> stories = new ArrayList<Story>(cursor.getCount());
		while(cursor.moveToNext()) {
			Story story = new Story(cursor);
			stories.add(story);
		}
		cursor.close();
		return stories;
	}

	public ArrayList<Story> getAll(String kidId) throws Exception {
		String sql = "SELECT * from " + DbHelper.TBL_STORY + " WHERE " + DbHelper.KID_ID + "=" + DatabaseUtils.sqlEscapeString(kidId) + " ORDER BY " + DbHelper.CREATED_TIME + " DESC";
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		ArrayList<Story> stories = new ArrayList<Story>(cursor.getCount());
		while (cursor.moveToNext()) {
			Story story = new Story(cursor);
			stories.add(story);
		}
		cursor.close();
		for (Story story : stories) {
			story.setAlbums(getAlbum(story.getId()));
		}
		
		return stories;
	}

	private ArrayList<RichMedia> getAlbum(String id) throws Exception {
		String sql = "SELECT * from " + DbHelper.TBL_STORY_RICH_MEDIA + " WHERE " + DbHelper.STORY_ID + "=" + DatabaseUtils.sqlEscapeString(id);
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		ArrayList<RichMedia> albums = new ArrayList<RichMedia>(cursor.getCount());
		while (cursor.moveToNext()) {
			RichMedia richMedia = new RichMedia(cursor);
			albums.add(richMedia);
		}
		cursor.close();
		return albums;
	}

	public void saveRichMedia(ArrayList<RichMedia> richMedia) throws Exception {
		if(richMedia == null || richMedia.size() == 0) return;
		String[] sqls = new String[richMedia.size()];
		int i = 0;
		for (RichMedia aMedia : richMedia) {
			sqls[i++] = aMedia.getInsertSql();
		}
		DbHelper.getInstance().execSQL(sqls);
	}

}
