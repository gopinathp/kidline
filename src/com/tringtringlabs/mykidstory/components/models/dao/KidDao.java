package com.tringtringlabs.mykidstory.components.models.dao;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.Kid;

public class KidDao implements BaseDao<Kid>{
	
	private static KidDao instance;

	private KidDao() {
		instance = this;
	}
	
	public static KidDao getInstance() {
		if(instance == null) {
			new KidDao();
		}
		return instance;
	}
	
	private static final String COMMA = " , ";

	// private static final String CREATE_TABLE_KID = CREATE_TABLE + TBL_KID +
	// "(" + OBJECT_ID + " TEXT PRIMARY KEY, " + NAME + " TEXT, " + DOB +
	// " INT, " + GENDER + " SMALLINT DEFAULT 1, " + EMAIL + " TEXT, " + FB_ID +
	// " TEXT, " + PHONE_NUMBER + " TEXT, " + JOB_TITLE + " TEXT, " + LOCATION +
	// " TEXT, " + LOCATION_LAT + " TEXT, " + LOCATION_LONG + " TEXT," +
	// BIRTH_SIGN + " TEXT, " + FATHER_ID + " TEXT, " + MOTHER_ID + " TEXT," +
	// IS_PRIME + " SMALLINT DEFAULT 0)";
	// private static final String CREATE_TABLE_KID_GROWTH = CREATE_TABLE +
	// TBL_KID_GROWTH + "(" + OBJECT_ID + " TEXT PRIMARY KEY, " + KID_ID +
	// " TEXT, " + MEASURED_DATE + " INT, " + HEIGHT_IN_CMS + " INT)";
	// private static final String CREATE_TABLE_KID_TEETH = CREATE_TABLE +
	// TBL_KID_TEETH + "(" + OBJECT_ID + " TEXT PRIMARY KEY, " + KID_ID +
	// " TEXT, " + MEASURED_DATE + " INT, " + NUMBER_OF_TEETH + " SMALLINT)";
	@Override
	public synchronized void add(Kid kid) throws Exception {
		String sql = kid.getInsertSql();
		DbHelper.getInstance().insertAsStatementWithBlob(sql, kid.getProfilePicture());
	}

	@Override
	public synchronized void update(String objectId, Kid kid) throws Exception {
		delete(objectId);
		add(kid);
	}

	@Override
	public void delete(String objectId) throws Exception {
		String sql = "DELETE FROM " + DbHelper.TBL_KID + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(objectId);
		DbHelper.getInstance().execSQL(sql);
	}

	@Override
	public void delete(Kid kid) throws Exception {
		delete(kid.getId());
	}

	@Override
	public Kid findById(String id) throws Exception {
		String sql = " SELECT * from " + DbHelper.TBL_KID + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(id);
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		if(cursor.moveToNext()) {
			Kid kid = new Kid(cursor);
			cursor.close();
			return kid;
		}
		return null;
	}

	@Override
	public ArrayList<Kid> getAll() throws Exception {
		String sql = " SELECT * from " + DbHelper.TBL_KID;
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		ArrayList<Kid> kids = new ArrayList<Kid>(cursor.getCount());
		while(cursor.moveToNext()) {
			Kid kid = new Kid(cursor);
			kids.add(kid);
		}
		cursor.close();
		return kids;
	}

	@Override
	public void add(ArrayList<Kid> kids) throws Exception {
		for (Kid kid : kids) {
			add(kid);
		}
	}
	
}
