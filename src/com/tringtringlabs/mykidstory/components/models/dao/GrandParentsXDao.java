package com.tringtringlabs.mykidstory.components.models.dao;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.GrandParentsX;

public class GrandParentsXDao implements BaseDao<GrandParentsX> {

	@Override
	public void add(GrandParentsX grandParent) throws Exception {
		String sql = grandParent.getInsertSql();
		DbHelper.getInstance().insertAsStatementWithBlob(sql, grandParent.getProfilePicture());
	}

	@Override
	public void add(ArrayList<GrandParentsX> grandParents) throws Exception {
		for (GrandParentsX aGrandParent : grandParents) {
			add(aGrandParent);
		}
	}

	@Override
	public void update(String primaryKey, GrandParentsX grandParent) throws Exception {
		delete(grandParent);
		add(grandParent);
	}

	@Override
	public void delete(GrandParentsX grandParent) throws Exception {
		delete(grandParent.getId());
	}

	@Override
	public void delete(String objectId) throws Exception {
		String sql = "DELETE FROM " + DbHelper.TBL_GRAND_PARENT_X + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(objectId);
		DbHelper.getInstance().execSQL(sql);
	}

	@Override
	public GrandParentsX findById(String id) throws Exception {
		String sql = "SELECT * FROM " + DbHelper.TBL_GRAND_PARENT_X + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(id);
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		if (cursor.moveToNext()) {
			GrandParentsX grandParentsX = new GrandParentsX(cursor);
			return grandParentsX;
		}
		return null;
	}

	@Override
	public ArrayList<GrandParentsX> getAll() throws Exception {
		String sql = "SELECT * FROM " + DbHelper.TBL_GRAND_PARENT_X;
		Cursor cursor = DbHelper.getInstance().rawQuery(sql);
		ArrayList<GrandParentsX> grandParents = new ArrayList<GrandParentsX>(cursor.getCount());
		while (cursor.moveToNext()) {
			GrandParentsX grandParentsX = new GrandParentsX(cursor);
			grandParents.add(grandParentsX);
		}
		return grandParents;
	}

}
