package com.tringtringlabs.mykidstory.components.models.dao;

import java.util.ArrayList;

public interface BaseDao<T> {
	public void add(T item) throws Exception;
	public void add(ArrayList<T> items) throws Exception;
	public void update(String primaryKey, T item) throws Exception;
	public void delete(T item) throws Exception;
	public void delete(String objectId) throws Exception;
	public T findById(String id) throws Exception;
	public ArrayList<T> getAll() throws Exception;
}
