package com.tringtringlabs.mykidstory.components.models.dao;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.ParentsX;
public class ParentsXDao implements BaseDao<ParentsX> {

	@Override
	public void add(ParentsX parent) throws Exception {
		String sql = parent.getInsertSql();
		DbHelper.getInstance().insertAsStatementWithBlob(sql, parent.getProfilePicture());
	}

	@Override
	public void add(ArrayList<ParentsX> parents) throws Exception {
		for (ParentsX parentsX : parents) {
			add(parentsX);
		}
	}

	@Override
	public void update(String primaryKey, ParentsX parent) throws Exception {
		delete(parent);
		add(parent);
	}

	@Override
	public void delete(ParentsX parent) throws Exception {
		delete(parent.getId());
	}

	@Override
	public void delete(String objectId) throws Exception {
		String sql = "DELETE FROM " + DbHelper.TBL_PARENT_X + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(objectId);
		DbHelper.getInstance().execSQL(sql);
	}

	@Override
	public ParentsX findById(String id) throws Exception {
		String query = "SELECT * FROM " + DbHelper.TBL_PARENT_X + " WHERE " + DbHelper.OBJECT_ID + "=" + DatabaseUtils.sqlEscapeString(id);
		Cursor cursor = DbHelper.getInstance().rawQuery(query);
		if(cursor.moveToNext()) {
			ParentsX parentsX = new ParentsX(cursor);
			return parentsX;
		}
		return null;
	}

	@Override
	public ArrayList<ParentsX> getAll() throws Exception {
		String query = "SELECT * FROM " + DbHelper.TBL_PARENT_X;
		Cursor cursor = DbHelper.getInstance().rawQuery(query);
		ArrayList<ParentsX> parentsXs = new ArrayList<ParentsX>(cursor.getCount());
		while(cursor.moveToNext()) {
			ParentsX parentsX = new ParentsX(cursor);
			parentsXs.add(parentsX);
		}
		return parentsXs;
	}

}
