package com.tringtringlabs.mykidstory.components.models;

import static com.tringtringlabs.mykidstory.Constants.COMMA;

import java.lang.reflect.Field;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.google.gson.Gson;
import com.parse.ParseObject;
import com.tringtringlabs.mykidstory.Constants;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.utils.parseable;

public class GrandParentsX extends Person implements parseable{
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
	public GrandParentsX(Cursor cursor) {
		super(cursor);
	}

	public String getInsertSql() {
		String sql = "INSERT INTO " + DbHelper.TBL_PARENT_X + " VALUES(" 
				+ getId() + Constants.COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getName())) + COMMA
				+ ((getProfilePicture() == null) ? "null" : " ? ") + COMMA
				+ getDateOfBirth().getTime() + COMMA
				+ getGender() + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getEmail())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getFbId())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getPhoneNumber())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getJobTitle())) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getLocation())) + COMMA
				+ (getLocationLat()) + COMMA
				+ (getLocationLong()) + COMMA
				+ getCreatedAt().getTime() + COMMA
				+ getUpdatedAt().getTime() + COMMA
				+")";
		return sql;
	}
	
	public ParseObject toParseObject() throws IllegalArgumentException, IllegalAccessException {
		ParseObject object = new ParseObject(this.getClass().getName());
		Field[] declaredFields = getClass().getDeclaredFields();
		for (Field field : declaredFields) {
			object.put(field.getName(), field.get(this));
		}
		return object;
	}

}
