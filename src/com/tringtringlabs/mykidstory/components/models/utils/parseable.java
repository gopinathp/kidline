package com.tringtringlabs.mykidstory.components.models.utils;

import com.parse.ParseObject;

public interface parseable {
	
	public ParseObject toParseObject() throws IllegalArgumentException, IllegalAccessException;

}
