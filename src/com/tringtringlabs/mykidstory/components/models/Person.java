package com.tringtringlabs.mykidstory.components.models;

import java.util.Date;

import android.database.Cursor;

import com.google.gson.Gson;
import com.tringtringlabs.mykidstory.components.DbHelper;

public class Person {

	public enum Gender {
		FEMALE, MALE
	}
	private Date createdAt;
	private Date dateOfBirth;
	// Extended information
	private String email;
	private String fbId;
	private Gender gender;
	// Basic information
	private String id;
	private String jobTitle;
	private String location;
	private double locationLat = -1;
	private double locationLong = -1;
	private String name;
	
	private String phoneNumber;

	private byte[] profilePicture;
	private Date updatedAt;
	public Person() {
	}
	
	public Person(Cursor cursor) {
		this.id = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.OBJECT_ID)));
		this.name = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.NAME)));
		this.gender = Gender.values()[cursor.getInt(cursor.getColumnIndex(DbHelper.GENDER))];
		this.dateOfBirth = new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.DOB)));
		this.email = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.EMAIL)));
		this.fbId = (cursor.getString(cursor.getColumnIndex(DbHelper.FB_ID)));
		this.location = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.LOCATION)));
		this.phoneNumber = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.PHONE_NUMBER)));
		this.locationLat = cursor.getDouble(cursor.getColumnIndex(DbHelper.LOCATION_LAT));
		this.locationLong = cursor.getDouble(cursor.getColumnIndex(DbHelper.LOCATION_LONG));
		this.profilePicture = cursor.getBlob(cursor.getColumnIndex(DbHelper.PROFILE_PICTURE));
		setCreatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.CREATED_TIME))));
		setUpdatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.UPDATED_TIME))));
	}
	
	
	
	public Person(String id, String name, Gender gender, Date dateOfBirth, String email, String fbId, String location, String phoneNumber,
			String jobTitle, double locationLat, double locationLong, byte[] profilePicture) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.email = email;
		this.fbId = fbId;
		this.location = location;
		this.phoneNumber = phoneNumber;
		this.jobTitle = jobTitle;
		this.locationLat = locationLat;
		this.locationLong = locationLong;
		this.profilePicture = profilePicture;
	}



	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public String getFbId() {
		return fbId;
	}

	public Gender getGender() {
		return gender;
	}

	public String getId() {
		return id;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public String getLocation() {
		return location;
	}

	public double getLocationLat() {
		return locationLat;
	}

	public double getLocationLong() {
		return locationLong;
	}

	public String getName() {
		return name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public byte[] getProfilePicture() {
		return profilePicture;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setLocationLat(double locationLat) {
		this.locationLat = locationLat;
	}



	public void setLocationLong(double locationLong) {
		this.locationLong = locationLong;
	}



	public void setName(String name) {
		this.name = name;
	}



	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}



	public void setProfilePicture(byte[] profilePicture) {
		this.profilePicture = profilePicture;
	}



	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}



	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
