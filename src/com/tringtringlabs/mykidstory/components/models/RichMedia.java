package com.tringtringlabs.mykidstory.components.models;

import java.lang.reflect.Field;
import java.util.Date;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.google.gson.Gson;
import com.parse.ParseObject;
import com.tringtringlabs.mykidstory.Constants;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.utils.parseable;

public class RichMedia implements parseable {
	public enum Type {
		IMAGE, VIDEO
	}

	private String id;
	private String storyId;
	private String ownerId;
	private String url;
	private String localFilePath;
	private Type type;
	private long duration;
	private long size;
	private Date createdAt;
	private Date updatedAt;
	private double latitude = -1;
	private double longitude = -1;

	public RichMedia(String id, String storyId, String ownerId, String url, String localFilePath, double latitude, double longitude, Type type, long duration, long size,
			Date dateTaken, Date updatedAt) {
		this.setId(id);
		this.setStoryId(storyId);
		this.ownerId = ownerId;
		this.url = url;
		this.setLocalFilePath(localFilePath);
		this.latitude = latitude;
		this.longitude = longitude;
		this.type = type;
		this.duration = duration;
		this.setSize(size);
		this.setCreatedAt(dateTaken);
		this.setUpdatedAt(updatedAt);
	}

	public RichMedia(Cursor cursor) {
		setId(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.OBJECT_ID))));
		setStoryId(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.STORY_ID))));
		this.ownerId = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.URL)));
		this.url = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.OWNER_ID)));
		this.localFilePath = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.LOCAL_FILE_PATH)));
		this.latitude = cursor.getDouble(cursor.getColumnIndex(DbHelper.LOCATION_LAT));
		this.longitude = cursor.getDouble(cursor.getColumnIndex(DbHelper.LOCATION_LONG));
		this.type = Type.values()[(cursor.getInt(cursor.getColumnIndex(DbHelper.TYPE)))];
		this.duration = cursor.getInt(cursor.getColumnIndex(DbHelper.DURATION));
		this.size = cursor.getInt(cursor.getColumnIndex(DbHelper.SIZE));
		this.createdAt = new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.CREATED_TIME)));
		this.updatedAt = new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.UPDATED_TIME)));
	}
	
	public String getInsertSql() {
		String sql = "INSERT INTO " + DbHelper.TBL_STORY_RICH_MEDIA + " VALUES("
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getId())) + Constants.COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getStoryId())) + Constants.COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getOwnerId())) + Constants.COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getUrl())) + Constants.COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getLocalFilePath())) + Constants.COMMA
						+ latitude + Constants.COMMA
						+ longitude + Constants.COMMA
						+ getType().ordinal() + Constants.COMMA
						+ getDuration() + Constants.COMMA
						+ getSize() + Constants.COMMA
						+ createdAt.getTime() + Constants.COMMA
						+ updatedAt.getTime()
						+ ")";
		return sql;
						

	}

	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	public ParseObject toParseObject() throws IllegalArgumentException, IllegalAccessException {
		ParseObject object = new ParseObject(this.getClass().getName());
		Field[] declaredFields = getClass().getDeclaredFields();
		for (Field field : declaredFields) {
			object.put(field.getName(), field.get(this));
		}
		return object;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLocalFilePath() {
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath) {
		this.localFilePath = localFilePath;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStoryId() {
		return storyId;
	}

	public void setStoryId(String storyId) {
		this.storyId = storyId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
