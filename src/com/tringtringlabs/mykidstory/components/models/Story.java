package com.tringtringlabs.mykidstory.components.models;

import static com.tringtringlabs.mykidstory.Constants.COMMA;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.ParseObject;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.utils.parseable;

public class Story implements parseable{
	private String kidId;
	private String id;
	private StoryType type;
	private String title;
	private String description;
	private Date date;
	private ArrayList<String> tags;
	private String location;
	private double locationLat = -1;
	private double locationLong = -1;
	private String fbPostId;
	private ArrayList<RichMedia> albums;
	private String otherAttributes;
	private Date createdAt;
	private Date updatedAt;
	
	public Story(Cursor cursor) {
		setId(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.OBJECT_ID))));
		kidId = DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.KID_ID)));
		type = StoryType.values()[cursor.getInt((cursor.getColumnIndex(DbHelper.STORY_TYPE)))];
		setTitle(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.TITLE))));
		setDescription(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.DESCRIPTION))));
		setDate(new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.STORY_DATE))));
		setTags(getTags(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.TAGS)))));
		setLocation(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.LOCATION))));
		setLocationLat((cursor.getDouble(cursor.getColumnIndex(DbHelper.LOCATION_LAT))));
		setLocationLong((cursor.getDouble(cursor.getColumnIndex(DbHelper.LOCATION_LONG))));
		setFbPostId(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.FB_POST_ID))));
		setOtherAttributes(DbHelper.nullCheckOut(cursor.getString(cursor.getColumnIndex(DbHelper.OTHER_ATTRS))));
		setCreatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.CREATED_TIME))));
		setUpdatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DbHelper.UPDATED_TIME))));
	}
	
	
	
	public Story(String id, String kidId, StoryType type, String title, String description, Date date, ArrayList<String> tags, String location,
			double locationLat, double locationLong, String fbPostId, ArrayList<RichMedia> albums, String otherAttributes, Date createdAt,
			Date updatedAt) {
		super();
		this.id = id;
		this.kidId = kidId;
		this.type = type;
		this.title = title;
		this.description = description;
		this.date = date;
		this.tags = tags;
		this.location = location;
		this.locationLat = locationLat;
		this.locationLong = locationLong;
		this.fbPostId = fbPostId;
		this.albums = albums;
		this.setOtherAttributes(otherAttributes);
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}



	private ArrayList<String> getTags(String jsonStr) {
		Type stringArrayListType = new TypeToken<Collection<String>>(){}.getType();
		Gson gson = new Gson();
		ArrayList<String> tags = null;
		try {
			tags = gson.fromJson(jsonStr, stringArrayListType);
		} catch (Exception e) {
		}
		return tags;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	public String getInsertSql() {
		String tagsJson = null;
		if(tags != null) {
			tagsJson = new Gson().toJson(tags);
		}
		String sql = "INSERT INTO " + DbHelper.TBL_STORY + " VALUES("
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getId())) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getKidId())) + COMMA
						+ type.ordinal() + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(title)) + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(description)) + COMMA
						+ date.getTime() + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(tagsJson)) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(location)) + COMMA
						+ locationLat + COMMA
						+ locationLong + COMMA
						+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(fbPostId)) + COMMA
				+ DatabaseUtils.sqlEscapeString(DbHelper.nullCheckIn(getOtherAttributes()))
				+ COMMA
						+ createdAt.getTime() + COMMA
						+ updatedAt.getTime()
						+ ")"						
						;
		return sql;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public String getFbPostId() {
		return fbPostId;
	}

	public void setFbPostId(String fbPostId) {
		this.fbPostId = fbPostId;
	}

	public ArrayList<RichMedia> getAlbums() {
		return albums;
	}

	public void setAlbums(ArrayList<RichMedia> albums) {
		this.albums = albums;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getLocationLat() {
		return locationLat;
	}

	public void setLocationLat(double locationLat) {
		this.locationLat = locationLat;
	}

	public double getLocationLong() {
		return locationLong;
	}

	public void setLocationLong(double locationLong) {
		this.locationLong = locationLong;
	}
	
	public ParseObject toParseObject() throws IllegalArgumentException, IllegalAccessException {
		ParseObject object = new ParseObject(this.getClass().getName());
		Field[] declaredFields = getClass().getDeclaredFields();
		for (Field field : declaredFields) {
			object.put(field.getName(), field.get(this));
		}
		return object;
	}

	public String getKidId() {
		return kidId;
	}



	public void setKidId(String kidId) {
		this.kidId = kidId;
	}

	public String getOtherAttributes() {
		return otherAttributes;
	}

	public void setOtherAttributes(String otherAttributes) {
		this.otherAttributes = otherAttributes;
	}
	
	
}
