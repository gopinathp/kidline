package com.tringtringlabs.mykidstory.components;
import android.graphics.Bitmap;
import android.util.LruCache;


public class MemCache {
	
	private static final int CACHE_SIZE_IMAGE = 4 * 1024 * 1024;
	public static LruCache<String, Bitmap> imageCache = new LruCache<String, Bitmap>(CACHE_SIZE_IMAGE) {
		protected int sizeOf(String key, Bitmap bitmap) {
			return bitmap.getByteCount();
		};
	};

}
