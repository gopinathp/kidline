package com.tringtringlabs.mykidstory;

import android.app.Application;
import android.content.Context;

import com.tringtringlabs.mykidstory.components.ParseManager;

public class App extends Application {
	
	private static App instance;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		ParseManager.initialize(); 
	}
	
	public static Context getContext() {
		return instance;
	}
	
	public static App getApp() {
		return instance;
	}

}
