package com.tringtringlabs.mykidstory.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.StackView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.cyrilmottier.polaris.PolarisMapView;
import com.tringtringlabs.mykidstory.Common;
import com.tringtringlabs.mykidstory.R;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.RichMedia;
import com.tringtringlabs.mykidstory.components.models.RichMedia.Type;
import com.tringtringlabs.mykidstory.components.models.Story;
import com.tringtringlabs.mykidstory.components.models.StoryType;
import com.tringtringlabs.mykidstory.components.models.dao.StoryDao;
import com.tringtringlabs.mykidstory.ui.view.LocationPickerView;
import com.tringtringlabs.mykidstory.ui.view.LocationPickerView.OnLocationSelectionListener;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

public class ComposeStoryActivity extends BaseMapActivity implements OnClickListener, OnDateSetListener, OnTimeSetListener, OnTouchListener, OnLocationSelectionListener {

	private static final int REQUEST_LOAD_IMAGES = 10;
	private EditText mTitleEditText;
	private EditText mDescriptionEditText;
	private Button mDateButton;
	private Button mTimeButton;
	private EditText mWithEditText;
	private EditText mGiftsEditText;
	private LocationPickerView mPickerView;
	private Calendar mEventCalendar;
	private StackView mStack;
	private ArrayList<RichMedia> mRichMedia;
	private RichMediaAdapter mRichMediaAdapter;
	private String mKidId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mKidId = getIntent().getStringExtra(DbHelper.KID_ID);
		setContentView(R.layout.activity_compose_story);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mTitleEditText = (EditText) findViewById(R.id.title_edittext);
		mDescriptionEditText = (EditText) findViewById(R.id.description_edittext);
		mDateButton = (Button) findViewById(R.id.date_edittext);
		mTimeButton = (Button) findViewById(R.id.time_edittext);
		mWithEditText = (EditText) findViewById(R.id.participants_edittext);
		mGiftsEditText = (EditText) findViewById(R.id.gifts_received_edittext);
		mPickerView = (LocationPickerView) findViewById(R.id.location_pickerview);
		mStack = (StackView) findViewById(R.id.richMediaStack);
		mTitleEditText.setOnClickListener(this);
		mTimeButton.setOnClickListener(this);
		mDateButton.setOnClickListener(this);
		findViewById(R.id.add_pics_button).setOnClickListener(this);
		findViewById(R.id.save_button).setOnClickListener(this);
		mPickerView.setOnLocationChoosenListener(this);
		
		mEventCalendar = Calendar.getInstance();
		mDateButton.setText(Common.getDateText(mEventCalendar.get(Calendar.YEAR), mEventCalendar.get(Calendar.MONTH), mEventCalendar.get(Calendar.DAY_OF_MONTH)));
		mTimeButton.setText(Common.getTimeText(mEventCalendar.get(Calendar.HOUR), mEventCalendar.get(Calendar.MINUTE)));
	}

	@Override
	protected void onStart() {
		super.onStart();
		PolarisMapView pMap = (PolarisMapView) mPickerView.findViewWithTag("gmap");
		pMap.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
		PolarisMapView pMap = (PolarisMapView) mPickerView.findViewWithTag("gmap");
		pMap.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_compose_story, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_pics_button:
			launchGallery();
			break;
		case R.id.save_button:
			handleStorySaving();
			break;
		case R.id.date_edittext:
			handleDateChanging();
			break;
		case R.id.time_edittext:
			handleTimeChanging();
			break;
		default:
			break;
		}
		
	}

	private void launchGallery() {
		Intent i = new Intent();
		i.setAction(Intent.ACTION_PICK);
		i.setType("image/*");
		i.setType("*/*");
		startActivityForResult(i, REQUEST_LOAD_IMAGES);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_LOAD_IMAGES && resultCode == RESULT_OK && null != data) {
			handlePhotoPickerResult(data);
		}
	}

	private void handlePhotoPickerResult(Intent data) {

		/*if(data.hasExtra(Intent.EXTRA_STREAM))*/ {
			//ArrayList<Parcelable> list = data.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
			if(mRichMedia == null)
				mRichMedia = new ArrayList<RichMedia>();
			Parcelable parcelable = data.getData();
			/*for (Parcelable parcelable : list)*/ {
				String[] filePathColumn = { MediaStore.Images.Media.DATA, MediaStore.Images.Media.MIME_TYPE, 
							MediaStore.Images.Media.DATE_TAKEN, MediaStore.Images.Media.LATITUDE, 
							MediaStore.Images.Media.LONGITUDE, MediaStore.MediaColumns.SIZE};
				Uri selectedImage = (Uri) parcelable;
				Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				String localFilePath = cursor.getString(0);
				String mimeType = cursor.getString(1);
				Type type = Type.IMAGE;
				if(!mimeType.contains("image")) {
					type = Type.VIDEO;
				}
				Date dateTaken = new Date(cursor.getLong(2));
				double latitude = cursor.getDouble(3);
				double longitude = cursor.getDouble(4);
				long size = cursor.getLong(5);
				long duration = 0;
				mRichMedia.add(new RichMedia(String.valueOf(new Date().getTime()), null, null, null, localFilePath, latitude, longitude, type, duration, size, dateTaken, dateTaken));
				cursor.close();
			}
			setupFlipper();
		}
	
		
	}

	private void setupFlipper() {
		if(mRichMediaAdapter == null) {
			mRichMediaAdapter = new RichMediaAdapter();
			mStack.setAdapter(mRichMediaAdapter);
		} else {
			mRichMediaAdapter.notifyDataSetChanged();
		}
	}

	private void handleDateChanging() {
		DatePickerDialog dialog = new DatePickerDialog(this, this, mEventCalendar.get(Calendar.YEAR), mEventCalendar.get(Calendar.MONTH),
				mEventCalendar.get(Calendar.DAY_OF_MONTH));
		dialog.show();
	}

	private void handleTimeChanging() {
		TimePickerDialog dialog = new TimePickerDialog(this, this, mEventCalendar.get(Calendar.HOUR_OF_DAY), mEventCalendar.get(Calendar.MINUTE),
				false);
		dialog.show();
	}

	private void handleStorySaving() {
		Location choosenLocation = mPickerView.getChoosenLocation();
		String locationAddress = mPickerView.getLocationAddress();
		Date createdAt = new Date();
		String id = String.valueOf(createdAt.getTime());
		StoryType type = StoryType.OTHERS;
		String title = mTitleEditText.getText().toString();
		String description = mDescriptionEditText.getText().toString();
		Date date = mEventCalendar.getTime();
		ArrayList<String> tags = new ArrayList<String>();
		double locationLat = -1;
		double locationLong = -1;
		if(choosenLocation != null) {
			locationLat = choosenLocation.getLatitude();
			locationLong = choosenLocation.getLongitude();
		}
		String fbPostId = null;
		Date updatedAt = createdAt;
		try {
			Story story = new Story(id, mKidId, type, title, description, date, tags, locationAddress, locationLat, locationLong, fbPostId, mRichMedia,
					null, createdAt, updatedAt);
			StoryDao.getInstance().add(
					story);
			if(mRichMedia != null)
			for (RichMedia media : mRichMedia) {
				media.setStoryId(story.getId());
				media.setId(String.valueOf(new Date().getTime()));
			}
			StoryDao.getInstance().saveRichMedia(mRichMedia);
		} catch (Exception e) {
			e.printStackTrace();
			Crouton.showText(this, "Exception while saving story." + e.getMessage(), Style.ALERT);
		}
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		mEventCalendar.set(Calendar.YEAR, year);
		mEventCalendar.set(Calendar.MONTH, monthOfYear);
		mEventCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		mDateButton.setText(Common.getDateText(year, monthOfYear, dayOfMonth));
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		mEventCalendar.set(Calendar.HOUR, hourOfDay);
		mEventCalendar.set(Calendar.MINUTE, minute);
		mTimeButton.setText(Common.getTimeText(hourOfDay, minute));
	}

	@Override
	public boolean onTouch(View v, MotionEvent e) {
		onClick(v);
		return true;
	}
	
	class RichMediaAdapter extends BaseAdapter {
		
		private LayoutInflater mInflater;

		public RichMediaAdapter() {
			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return mRichMedia.size();
		}

		@Override
		public Object getItem(int position) {
			return mRichMedia.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = mInflater.inflate(R.layout.show_rich_media, null);
			}
			
			fillDetails(convertView, (RichMedia)getItem(position));
			
			return convertView;
		}

		private void fillDetails(View convertView, RichMedia media) {
			ImageView picImageView = (ImageView) convertView.findViewById(R.id.pic_imageview);
			TextView infoTextView = (TextView) convertView.findViewById(R.id.info_textview);
			
			picImageView.setImageBitmap(Common.getBitmap(media.getLocalFilePath()));
			infoTextView.setText(media.toString());
		}
		
	}

	@Override
	public void onLocationSelected(Location location) {
		String locationAddress = mPickerView.getLocationAddress();
		Crouton.showText(this, "Selected location: " + locationAddress + " @ lat " + location.getLatitude() + ", long " + location.getLongitude(), Style.CONFIRM);
	}

}
