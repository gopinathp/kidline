package com.tringtringlabs.mykidstory.ui.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cyrilmottier.polaris.Annotation;
import com.cyrilmottier.polaris.PolarisMapView;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.tringtringlabs.mykidstory.Common;
import com.tringtringlabs.mykidstory.R;
import com.tringtringlabs.mykidstory.components.GLogger;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

public class LocationPickerView extends LinearLayout implements OnClickListener, OnLongClickListener {

	private Location mLocation;
	private String mLocationAddress;
	
	public interface OnLocationSelectionListener {
		void onLocationSelected(Location location);
	}

	private ImageView mPickButton;
	private PolarisMapView mMapView;
	private EditText mEditText;

	Mode currentMode = Mode.NOT_PICKING;
	enum Mode {
		NOT_PICKING,
		PICKING
	}

	public LocationPickerView(Context context) {
		super(context);
		init(context);
	}

	public LocationPickerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		LayoutInflater.from(context).inflate(R.layout.location_picker_layout, this, true);
		if (isInEditMode())
			return;
		mPickButton = (ImageView) findViewById(R.id.pick_from_map_button);
		mPickButton.setOnClickListener(this);
		mMapView = (PolarisMapView) findViewById(R.id.polaris_map_view);
		mEditText = (EditText) findViewById(R.id.location_edittext);
		mMapView.setActionnable(true);
		mMapView.setOnLongClickListener(this);
		mMapView.setUserTrackingButtonEnabled(true);
		mMapView.setBuiltInZoomControls(true);
		// final MyLocationOverlay locationOverlay = new
		// MyLocationOverlay(context, mMapView);
		// locationOverlay.enableCompass();
		// locationOverlay.enableMyLocation();
		// locationOverlay.runOnFirstFix(new Runnable() {
		// public void run() {
		// mMapView.getController().animateTo(locationOverlay.getMyLocation());
		// mMapView.removeAllOverlays();
		// mMapView.addOverlay(new MyOverlay());
		// }
		// });
		MyOverlay overlay = new MyOverlay();
		mMapView.addOverlay(overlay);
	}

	private double mLat;
	private double mLongi;

	class MyOverlay extends Overlay {

		@Override
		public boolean onTouchEvent(MotionEvent event, MapView mapView) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				GeoPoint geoPoint = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
				mLat = geoPoint.getLatitudeE6() / (1e6);
				mLongi = geoPoint.getLongitudeE6() / (1e6);
			}
			gestureDetector.onTouchEvent(event);
			return super.onTouchEvent(event, mapView);
		}
	}

	final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
		public void onLongPress(MotionEvent e) {
			GLogger.e("Longpress detected");
			handleLocationSelection();
		}
	});
	private List<OnLocationSelectionListener> mListeners = new ArrayList<LocationPickerView.OnLocationSelectionListener>();

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.pick_from_map_button:
			handlePickFromMapButtonClick();
			break;

		default:
			break;
		}

	}

	private void handlePickFromMapButtonClick() {
		Common.hideKeyBoard((Activity) getContext());
		if (currentMode == Mode.NOT_PICKING) {
			mMapView.setVisibility(View.VISIBLE);
			currentMode = Mode.PICKING;
		} else {
			mMapView.setVisibility(View.GONE);
			currentMode = Mode.NOT_PICKING;
		}
	}

	@Override
	public boolean onLongClick(View v) {
		if(v.getId() == mMapView.getId()) {
			handleLocationSelection();
		}
		return true;
	}

	class FindLocationTask extends AsyncTask<Object, Object, Object> {


		@Override
		protected Object doInBackground(Object... params) {
			// double lat = (Double) params[0];
			// double longi = (Double) params[1];
			Geocoder geocoder = new Geocoder(getContext());
			try {
				List<Address> list = geocoder.getFromLocation(mLat, mLongi, 1);
				if (list != null && list.size() > 0) {
					return list.get(0);
				}
			} catch (IOException e) {
				Crouton.showText((Activity) getContext(), "Failed to geocode your location", Style.ALERT);
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Object result) {
			if (result == null) {
				Crouton.showText((Activity) getContext(), "Failed to find the location", Style.ALERT);
				return;
			}
			Address address = (Address) result;
			List<Annotation> annotations = new ArrayList<Annotation>();
			GeoPoint point = new GeoPoint((int) (mLat * 1e6), (int) (mLongi * 1e6));
			String addressLine1 = address.getAddressLine(0);
			annotations.add(new Annotation(point, "Event location is chosen to be here.", "Location: " + addressLine1 + ", "
					+ address.getAdminArea()));
			mMapView.setAnnotations(annotations, R.drawable.map_pin_holed_blue);
			String displayLocation = addressLine1 + address.getAdminArea();
			mLocation = new Location(displayLocation);
			setLocationAddress(displayLocation);
			mLocation.setLatitude(address.getLatitude());
			mLocation.setLongitude(address.getLongitude());
			mEditText.setText(displayLocation);
			for (OnLocationSelectionListener aListener : mListeners) {
				aListener.onLocationSelected(mLocation);
			}
		}

	}
	
	public Location getChoosenLocation() {
		return mLocation;
	}

	private void handleLocationSelection() {
		new FindLocationTask().execute();
	}
	
	public void setOnLocationChoosenListener(OnLocationSelectionListener listener) {
		mListeners.add(listener);
	}

	public String getLocationAddress() {
		return mLocationAddress;
	}

	public void setLocationAddress(String locationAddress) {
		mLocationAddress = locationAddress;
	}

}
