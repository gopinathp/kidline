package com.tringtringlabs.mykidstory.ui;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import com.tringtringlabs.mykidstory.R;
import com.tringtringlabs.mykidstory.components.models.Kid;
import com.tringtringlabs.mykidstory.components.models.Person.Gender;
import com.tringtringlabs.mykidstory.components.models.dao.KidDao;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

public class AddKidActivity extends FragmentActivity implements OnClickListener {

	private static final int SELECT_PICTURE = 10;
	private EditText nameEditText;
	private ImageView profilePictureView;
	private Switch genderSwitch;
	private EditText birthDateEditText;
	private EditText birthTimeEditText;

	private Date dateOfBirth;
	private byte[] profilePicBytes;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_add_kid_basicinfo);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		// Show the Up button in the action bar.
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		nameEditText = (EditText) findViewById(R.id.nameEditText);
		profilePictureView = (ImageView) findViewById(R.id.profilePictureView);
		genderSwitch = (Switch) findViewById(R.id.genderSwitch);
		birthDateEditText = (EditText) findViewById(R.id.dateEditText);
		birthTimeEditText = (EditText) findViewById(R.id.timeEditText);
		profilePictureView.setOnClickListener(this);
		findViewById(R.id.saveButton).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.activity_add_kid, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_done:
			Crouton.makeText(this, "Coming Soon", Style.INFO).show();
			handleDoneButtonClick();
		}
		return super.onOptionsItemSelected(item);
	}

	private void handleDoneButtonClick() {
		if(!isInputValid()) {
			return;
		}
		String name = nameEditText.getText().toString();
		Gender gender = genderSwitch.isChecked()? Gender.MALE : Gender.FEMALE;
		dateOfBirth = new Date();
		String email = null;
		String location = null;
		double locationLat = -1;
		double locationLong = -1;
		String birthSign = null;
		String fatherId = null;
		String motherId = null;
		double birthLocLong = -1;
		double birthLocLat = -1;
		String birthLocation = null;
		Kid kid = new Kid(String.valueOf(System.currentTimeMillis()), name, gender, dateOfBirth, email, null, location, null, null, locationLat, locationLong, profilePicBytes, true, birthSign, fatherId, motherId, null, null, birthLocation, birthLocLat, birthLocLong);
		try {
			KidDao.getInstance().add(kid);
			finish();
		} catch (Exception e) {
			e.printStackTrace();
			Crouton.showText(this, e.getMessage(), Style.ALERT);
		}
	}

	private boolean isInputValid() {
		boolean isValid = true;
		if(nameEditText.getText().length() == 0) {
			isValid = false;
			warnUser("Name cannot be empty");
		}
		return isValid;
	}

	private void warnUser(String string) {
		Crouton.makeText(this, string, Style.ALERT).show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.profilePictureView:
			launchGallery();
			break;
		case R.id.saveButton:
			handleDoneButtonClick();
			break;

		default:
			break;
		}
		
	}

	private void launchGallery() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.putExtra("crop", "true");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode == RESULT_OK) {
	        if (requestCode == SELECT_PICTURE) {
	        	Bitmap bitmap = data.getExtras().getParcelable("data");
	        	profilePictureView.setImageBitmap(bitmap);
	        	ByteArrayOutputStream oStream = new ByteArrayOutputStream();
	        	bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
	            profilePicBytes = oStream.toByteArray();
	        }
	    }
	}
}
