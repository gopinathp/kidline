package com.tringtringlabs.mykidstory.ui;

import java.util.ArrayList;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.tringtringlabs.mykidstory.App;
import com.tringtringlabs.mykidstory.BuildConfig;
import com.tringtringlabs.mykidstory.Common;
import com.tringtringlabs.mykidstory.R;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.GLogger;
import com.tringtringlabs.mykidstory.components.models.Kid;
import com.tringtringlabs.mykidstory.components.models.Story;
import com.tringtringlabs.mykidstory.components.models.dao.KidDao;
import com.tringtringlabs.mykidstory.components.models.dao.StoryDao;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

public class KidFeedActivity extends FragmentActivity implements ActionBar.OnNavigationListener {

	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	private static final int MENU_COPY_FILES = 99;
	private ArrayList<Kid> kids;
	private StoriesCarouselHelper mCarouselHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kid_feed);
	}

	private void initialize() {
		try {
			kids = KidDao.getInstance().getAll();
		} catch (Exception e) {
			GLogger.d("Error getting the kids list from database" + e.getMessage());
			e.printStackTrace();
		}
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);

		if (kids != null && kids.size() > 0) {
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			actionBar.setListNavigationCallbacks(
			// Specify a SpinnerAdapter to populate the dropdown list.
					new ArrayAdapter<Kid>(actionBar.getThemedContext(), android.R.layout.simple_list_item_1, android.R.id.text1, kids), this);
		}
	}

	@Override
	public boolean onSearchRequested() {
		Common.comingSoon(this);
		return super.onSearchRequested();
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_kid_feed, menu);
		MenuItem item = menu.findItem(R.id.menu_delete_kid);
		if (BuildConfig.DEBUG) {
			menu.add(Menu.NONE, MENU_COPY_FILES, Menu.NONE, "Copy App Files to sdcard");
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_add_story:
			handleAddStoryOption();
			break;
		case R.id.menu_add_kid:
			startActivity(new Intent(this, AddKidActivity.class));
			break;
		case R.id.menu_delete_kid:
			deleteCurrentKid();
			initialize();
			break;
		case R.id.menu_settings:
			Common.comingSoon(this);
			break;
		case MENU_COPY_FILES:
			Common.handleCopyFilesToSdCard(App.getContext());
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		initialize();
		if (mCarouselHelper != null) {
			mCarouselHelper.onResume();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mCarouselHelper != null) {
			mCarouselHelper.onPause();
		}
	}

	private void handleAddStoryOption() {
		if (kids == null || kids.size() == 0) {
			Crouton.showText(this, "Add a kid first to get started", Style.INFO);
			return;
		}
		Intent intent = new Intent(this, ComposeStoryActivity.class);
		String kidId = kids.get(getActionBar().getSelectedNavigationIndex()).getId();
		intent.putExtra(DbHelper.KID_ID, kidId);
		startActivity(intent);
	}

	private void deleteCurrentKid() {
		try {
			if (kids == null || kids.size() == 0) {
				return;
			}
			Kid kid = kids.get(getActionBar().getSelectedNavigationIndex());
			KidDao.getInstance().delete(kid);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onNavigationItemSelected(int position, long id) {
		String kidId = kids.get(position).getId();
		try {
			ArrayList<Story> stories = StoryDao.getInstance().getAll(kidId);
			mCarouselHelper = new StoriesCarouselHelper(this, stories);
		} catch (Exception e) {
			e.printStackTrace();
			Crouton.showText(this, "Exception: " + e.getMessage(), Style.ALERT);
		}
		return true;
	}
}
