package com.tringtringlabs.mykidstory.ui;

import android.app.Activity;
import android.os.Bundle;

import com.tringtringlabs.mykidstory.components.GLogger;

public class BaseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		GLogger.d("inside onCreate()" + this.getClass().getSimpleName());
	}

	@Override
	protected void onStart() {
		super.onStart();
		GLogger.d("inside onStart()" + this.getClass().getSimpleName());
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		GLogger.d("inside onRestart()" + this.getClass().getSimpleName());
	}

	@Override
	protected void onResume() {
		super.onResume();
		GLogger.d("inside onResume()" + this.getClass().getSimpleName());
	}

	@Override
	protected void onStop() {
		super.onStop();
		GLogger.d("inside onstop()" + this.getClass().getSimpleName());
	}

	@Override
	protected void onPause() {
		super.onPause();
		GLogger.d("inside onPause()" + this.getClass().getSimpleName());
	}

}
