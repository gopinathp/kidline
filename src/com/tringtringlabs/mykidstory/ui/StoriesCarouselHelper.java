package com.tringtringlabs.mykidstory.ui;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Shader;
import android.widget.Toast;

import com.android.ex.carousel.CarouselView;
import com.android.ex.carousel.CarouselViewHelper;
import com.tringtringlabs.mykidstory.App;
import com.tringtringlabs.mykidstory.Common;
import com.tringtringlabs.mykidstory.Constants;
import com.tringtringlabs.mykidstory.R;
import com.tringtringlabs.mykidstory.components.DbHelper;
import com.tringtringlabs.mykidstory.components.models.RichMedia;
import com.tringtringlabs.mykidstory.components.models.Story;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

public class StoriesCarouselHelper {

	public static int TEXTURE_HEIGHT = 512;
	public static int TEXTURE_WIDTH = 512;

	protected static final boolean DBG = true;
	private static final int DETAIL_TEXTURE_WIDTH = 200;
	private static final int DETAIL_TEXTURE_HEIGHT = 20;
	private static final int[] arrayOfInt = {255,0,0};

	private CarouselView mView;
	private Paint mPaint = new Paint();
	private CarouselViewHelper mHelper;
	private Bitmap mGlossyOverlay;
	private Bitmap mDetailLineBitmap;
	private Bitmap mBorder;
	private ArrayList<Story> stories;
	
	private static int[] createColors() {
        int STRIDE = 2;
		int HEIGHT = 2;
		int[] colors = new int[STRIDE * HEIGHT];
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < STRIDE; x++) {
                int r = x * 255 / (STRIDE - 1);
                int g = y * 255 / (HEIGHT - 1);
                int b = 255 - Math.min(r, g);
                int a = Math.max(r, g);
                colors[y * STRIDE + x] = (a << 24) | (r << 16) | (g << 8) | b;
            }
        }
        return colors;
    }

	class LocalCarouselViewHelper extends CarouselViewHelper {
		private static final int PIXEL_BORDER = 3;
		private DetailTextureParameters mDetailTextureParameters = new DetailTextureParameters(5.0f, 5.0f, 3.0f, 10.0f);

		LocalCarouselViewHelper(Context context) {
			super(context);
		}

		@Override
		public void onCardSelected(final int id) {
			handleCardSelection(id);
		}

		@Override
		public void onDetailSelected(final int id, int x, int y) {
			handleCardSelection(id);
		}

		@Override
		public void onCardLongPress(int n, int touchPosition[], Rect detailCoordinates) {
			handleCardSelection(n);
		}

		@Override
		public DetailTextureParameters getDetailTextureParameters(int id) {
			return mDetailTextureParameters;
		}

		@Override
		public Bitmap getTexture(int n) {
			Story story = stories.get(n);
			ArrayList<RichMedia> richMedii = story.getAlbums();
			RichMedia media = null;
			Bitmap bitmap;
			Canvas canvas;
			if (richMedii != null && richMedii.size() > 0) {
				// TODO get the favorite rich media item of this story instead
				// of the first pic.
				media = richMedii.get(0);
				bitmap = Common.getBitmap(media.getLocalFilePath());
				canvas = new Canvas(bitmap);
			} else {
				bitmap = Bitmap.createBitmap(TEXTURE_WIDTH, TEXTURE_HEIGHT, Bitmap.Config.ARGB_8888);
				canvas = new Canvas(bitmap);
				canvas.drawBitmap(mGlossyOverlay, null,
						 new Rect(PIXEL_BORDER, PIXEL_BORDER, TEXTURE_WIDTH -
						 PIXEL_BORDER, TEXTURE_HEIGHT - PIXEL_BORDER), mPaint);
			}
			
			canvas.drawARGB(0, 0, 0, 0);
			mPaint.setColor(0x40808080);
			//canvas.drawRect(2, 2, TEXTURE_WIDTH - 2, TEXTURE_HEIGHT - 2, mPaint);
			mPaint.setTextSize(50.0f);
			mPaint.setAntiAlias(true);
			mPaint.setColor(0xffffffff);
			canvas.drawText("" + story.getDescription(), TEXTURE_WIDTH/2 - 10, TEXTURE_HEIGHT - 10, mPaint);
//			canvas.drawBitmap(mGlossyOverlay, null,
//			 new Rect(PIXEL_BORDER, PIXEL_BORDER, TEXTURE_WIDTH -
//			 PIXEL_BORDER, TEXTURE_HEIGHT - PIXEL_BORDER), mPaint);
			return bitmap;
		}

		@Override
		public Bitmap getDetailTexture(int n) {
			Bitmap bitmap = Bitmap.createBitmap(DETAIL_TEXTURE_WIDTH, DETAIL_TEXTURE_HEIGHT, Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			canvas.drawARGB(64, 10, 10, 10);
			mPaint.setTextSize(12.0f);
			mPaint.setAntiAlias(true);
			Story story = stories.get(n);
			canvas.drawText(story.getTitle() + " , " + Common.getRelativeDate(story.getDate()), 0, DETAIL_TEXTURE_HEIGHT / 2, mPaint);
			return bitmap;
		}
	};

	private void handleCardSelection(final int n) {
		mActivityReference.get().runOnUiThread(new Runnable() {
			public void run() {
				Intent intent = new Intent(mActivityReference.get(), PresentationActivity.class);
				intent.putExtra(DbHelper.STORY_ID, stories.get(n).getId());
				mActivityReference.get().startActivity(intent);
			}
		});

	}

	public StoriesCarouselHelper(Activity activity, ArrayList<Story> stories) {
		mActivityReference = new WeakReference<Activity>(activity);
		this.stories = stories;
		onCreate();
	}

	public void onCreate() {
		Resources resources = mActivityReference.get().getResources();
		int CARD_SLOTS = resources.getInteger(R.integer.carousel_card_slots);
		int TOTAL_CARDS = stories.size();
		final int VISIBLE_DETAIL_COUNT = resources.getInteger(R.integer.carousel_visible_details);
		int SLOTS_VISIBLE = resources.getInteger(R.integer.carousel_visible_slots);
		if (SLOTS_VISIBLE > stories.size()) {
			SLOTS_VISIBLE = stories.size();
		}
		mView = (CarouselView) mActivityReference.get().findViewById(R.id.carousel);
		mView.getHolder().setFormat(PixelFormat.RGBA_8888);
		mPaint.setColor(0xffffffff);
		final Resources res = resources;

		mHelper = new LocalCarouselViewHelper(mActivityReference.get());
		mHelper.setCarouselView(mView);
		mView.setPrefetchCardCount(4);
		mView.setSlotCount(CARD_SLOTS);
		mView.createCards(TOTAL_CARDS);
		mView.setVisibleSlots(SLOTS_VISIBLE);
		mView.setStartAngle((float) -(2.0f * Math.PI * 5 / CARD_SLOTS));
		mBorder = BitmapFactory.decodeResource(res, R.drawable.border);
		mDetailLineBitmap = BitmapFactory.decodeResource(resources, R.drawable.vertical_red_line);
		mView.setDefaultBitmap(mBorder);
		mView.setLoadingBitmap(mBorder);
		mView.setBackgroundColor(0.25f, 0.25f, 0.5f, 0.5f);
		mView.setRezInCardCount(3.0f);
		mView.setFadeInDuration(250);
		mView.setDetailLineBitmap(mDetailLineBitmap);
		mView.setVisibleDetails(VISIBLE_DETAIL_COUNT);
		mView.setDragModel(CarouselView.DRAG_MODEL_PLANE);
		mView.setDefaultBitmap(BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_web));
		// TODO fix the issue with glossy
		mGlossyOverlay = BitmapFactory.decodeResource(res, R.drawable.glossy_overlay);

	}

	private WeakReference<Activity> mActivityReference;

	public void onResume() {
		mHelper.onResume();
	}

	public void onPause() {
		mHelper.onPause();
	}

}
